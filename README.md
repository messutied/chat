# Chat Challenge App

Chat app built with Node.js and React.js, check it out at [chat.edd.bz](http://chat.edd.bz).

The usage of the chat is simple, start a new chat and get a Room URL, share this URL to anybody that wants to join you in the chat room.

When opening a Room URL you enter your user name and start chatting with anybody present in that room.

## Requirements

1. Node.js >= 6.0
2. Yarn

## Setup

Run `yarn` to install dependencies

## Development

Run `npm run dev` and the server will be up in port 3000  with hot reloading enabled.

## Testing

Run `npm run test` to run tests

## Production

- Run `npm run build` to build the production ready bundle
- Run `NODE_ENV=production [PORT=<port-number>] npm start` to start the server in production mode
