const crypto = require('crypto');

module.exports = class Room {
  constructor(io) {
    this.key = crypto.randomBytes(16).toString('hex');
    // dictionary: { socketId: userName } to be able to remove an userName
    // on socket disconnect with delete users[socketId]
    this.users = {};
    this.messages = [];
    this.nsp = io.of(`/${this.key}`);

    this.setupWebSocket();
  }

  setupWebSocket() {
    this.nsp.on('connection', (socket) => {
      socket.on('login', userName => this.addUser(socket, userName));
      socket.on('mssg', (mssg) => {
        const color = this.users[socket.id] ? this.users[socket.id].color : null;
        const time = (new Date() + '').split('GMT')[0];
        const fullMssg = { userName: mssg.userName, content: mssg.content, color, time };
        this.messages.push(fullMssg);
        this.nsp.emit('mssg', fullMssg);
      });
      socket.on('disconnect', () => this.removeUser(socket));
    });
  }

  static getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i += 1) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  addUser(socket, userName) {
    if (this.usersList().includes(userName)) {
      this.addUser(socket, `${userName}*`);
    } else {
      const color = Room.getRandomColor();
      this.users[socket.id] = { userName, color };
      socket.emit('userName', this.users[socket.id]);
      this.sendUsers();
    }
  }

  removeUser(socket) {
    delete this.users[socket.id];
    this.sendUsers();
  }

  sendUsers() {
    this.nsp.emit('users', this.usersList());
  }

  usersList() {
    return Object.keys(this.users).map(k => this.users[k]);
  }

  toJSON() {
    return { key: this.key, messages: this.messages };
  }
};
