const Room = require('./Room');

module.exports = class Chat {
  constructor(io) {
    this.io = io;
    this.rooms = [];
  }

  getRoom(key) {
    return this.rooms.find(room => room.key === key);
  }

  setupRoom() {
    const room = new Room(this.io);
    this.rooms.push(room);
    return room;
  }
};
