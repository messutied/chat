const TEST_SERVER_PORT = 8010;
const TEST_SERVER_URL = `http://localhost:${TEST_SERVER_PORT}`;

module.exports = { TEST_SERVER_PORT, TEST_SERVER_URL };
