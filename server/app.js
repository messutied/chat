const express = require('express');
const url = require('url');
const path = require('path');
const { Server } = require('http');
const SocketIO = require('socket.io');
const Chat = require('./Chat');

const app = express();
const http = new Server(app);
const io = new SocketIO(http);
const chat = new Chat(io);

if (process.env.NODE_ENV === 'development') {
  // pull assets from dev server (to enable hot-reloading)
  app.use('/assets', require('proxy-middleware')(url.parse('http://localhost:8080/assets')));
} else {
  // pull assets from static files
  app.use('/assets', express.static(path.resolve(__dirname, '../www/assets')));
}

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../www/index.html'));
});

app.post('/rooms', (req, res) => {
  const room = chat.setupRoom();
  res.json(room.toJSON());
});

app.get('/rooms/:key', (req, res) => {
  const room = chat.getRoom(req.params.key);
  if (room) res.json(room.toJSON());
  else res.sendStatus(404);
});

module.exports = http;
