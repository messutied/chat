const app = require('./app');

const ENV = process.env.NODE_ENV;
const PORT = process.env.PORT || (ENV !== 'test' ? 3000 : 8010);

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));
