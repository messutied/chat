const assert = require('assert');
const fetch = require('node-fetch');
const io = require('socket.io-client');
const { TEST_SERVER_PORT, TEST_SERVER_URL } = require('../server/constants');

process.env.NODE_ENV = 'test';

describe('Chat', () => {
  let key;

  before((done) => {
    this.server = require('../server/app');
    this.server.listen(TEST_SERVER_PORT, done);
  });

  beforeEach((done) => {
    fetch(`${TEST_SERVER_URL}/rooms`, { method: 'POST' })
      .then(resp => resp.json())
      .then((room) => {
        key = room.key;
        done();
      });
  });

  it('send me my user name and random color when I login', (done) => {
    const socket = io(`${TEST_SERVER_URL}/${key}`);
    socket.on('userName', (user) => {
      assert.equal(user.userName, 'Ed');
      assert.equal(user.color.length, 7);
      assert.equal(user.color[0], '#');
      done();
    });
    socket.emit('login', 'Ed');
  });

  it('broadcast the active users when a new one logins', (done) => {
    const socket1 = io(`${TEST_SERVER_URL}/${key}`);
    socket1.on('users', () => { // first time it broadcast my own name
      const socket2 = io(`${TEST_SERVER_URL}/${key}`);
      socket1.on('users', (users) => { // when second user joins I receive his name too
        assert.deepEqual(users.map(u => u.userName), ['Ed', 'Ed2']);
        done();
      });
      socket2.emit('login', 'Ed2');
    });
    socket1.emit('login', 'Ed');
  });

  after((done) => {
    this.server.close();
    setTimeout(() => done(), 100);
  });
});
