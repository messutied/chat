const assert = require('assert');
const fetch = require('node-fetch');
const { TEST_SERVER_PORT, TEST_SERVER_URL } = require('../server/constants');

process.env.NODE_ENV = 'test';

describe('Server', () => {
  let key;

  before((done) => {
    this.server = require('../server/app');
    this.server.listen(TEST_SERVER_PORT, done);
  });

  it('should create a new chat room with a randomly generated key', (done) => {
    fetch(`${TEST_SERVER_URL}/rooms`, { method: 'POST' })
      .then(resp => resp.json())
      .then((room) => {
        key = room.key;
        assert.equal(room.key.length, 32);
        done();
      });
  });

  it('should return 200 when fetching existing room', (done) => {
    fetch(`${TEST_SERVER_URL}/rooms/${key}`)
      .then((resp) => {
        console.log(resp.status);
        assert.equal(resp.status, 200);
        done();
      });
  });

  it('should return 404 when fetching non existing room', (done) => {
    fetch(`${TEST_SERVER_URL}/rooms/<non-existing-room>`)
      .then((resp) => {
        assert.equal(resp.status, 404);
        done();
      });
  });

  after((done) => {
    this.server.close();
    setTimeout(() => done(), 100);
  });
});
