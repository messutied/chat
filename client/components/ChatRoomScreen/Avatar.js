import React from 'react';

export default function Avatar(props) {
  return (
    <div className="message--user-name" style={{ backgroundColor: props.color }}>
      {props.name[0].toUpperCase()}
    </div>
  );
}
