import React, { Component, PropTypes } from 'react';
import io from 'socket.io-client';
import ChatRoom from './ChatRoom';
import './ChatRoomScreen.scss';

export default class ChatRoomScreen extends Component {
  state = { message: '', messages: [], users: [] };

  componentDidMount() {
    this.socket = io(`/${this.props.chatKey}`);
    this.socket.emit('login', this.props.userName);

    this.socket.on('userName', user => this.props.setUsername(user.userName));
    this.socket.on('users', users => this.setState({ users }));
    this.socket.on('mssg', (mssg) => {
      this.setState({ messages: [...this.state.messages, mssg] });
    });
  }

  handleSubmit(evt) {
    evt.preventDefault();
    const { userName } = this.props;
    const { message } = this.state;
    this.socket.emit('mssg', { userName, content: message });
    this.setState({ message: '' });
  }

  render() {
    const { message, messages, users } = this.state;
    const { userName } = this.props;
    return (
      <ChatRoom
        onlineUsers={users}
        messages={this.props.messages.concat(messages)}
        userName={userName}
        setCurrentMessage={mssg => this.setState({ message: mssg })}
        currentMessage={message}
        handleSubmit={e => this.handleSubmit(e)}
      />
    );
  }
}

ChatRoomScreen.propTypes = {
  userName: PropTypes.string.isRequired,
  chatKey: PropTypes.string.isRequired,
  setUsername: PropTypes.func.isRequired,
  messages: PropTypes.array.isRequired,
};
