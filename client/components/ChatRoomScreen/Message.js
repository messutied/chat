import React, { Component, PropTypes } from 'react';
import Avatar from './Avatar';

export default class Message extends Component {
  render() {
    const { userName, color, time } = this.props;
    return (
      <div className={`message ${this.props.isMine ? 'mine' : ''}`}>
        <div className="message-inner-container">
          <Avatar name={userName} color={color} />
          <div className="message--content">
            <div className="upper-container">
              <div className="full-user-name">
                {userName}
              </div>
              <div className="timestamp">
                {time}
              </div>
            </div>
            <div className="message-text">
              {this.props.content}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Message.propTypes = {
  userName: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  isMine: PropTypes.bool.isRequired,
};
