import React, { PropTypes, Component } from 'react';
import Message from './Message';
import Avatar from './Avatar';

export default class ChatRoom extends Component {
  render() {
    const { onlineUsers, messages, handleSubmit, userName, currentMessage,
            setCurrentMessage } = this.props;
    return (
      <div className="chat-room-screen">
        <div className="online-users-container">
          <div>Online Users:</div>
          <div className="online-users">
            {
              onlineUsers.map(user =>
                <div key={user.userName} className="online-users--user">
                  <Avatar name={user.userName} color={user.color} />
                  <span>{user.userName}</span>
                </div>)
            }
          </div>
        </div>
        <div className="right-column">
          <div className="messages-container">
            <div className="messages">
              {
                messages.map((mssg, i) =>
                  <Message
                    key={i}
                    userName={mssg.userName}
                    color={mssg.color}
                    content={mssg.content}
                    time={mssg.time}
                    isMine={mssg.userName === userName}
                  />)
              }
            </div>
          </div>
          <form
            className="chat-input-form"
            onSubmit={(e) => { handleSubmit(e); this.input.focus(); }}
          >
            You (<b>{userName}</b>):&nbsp;
            <input
              ref={(e) => { this.input = e; }}
              type="text"
              className="user-name"
              placeholder="Enter your amazing message here"
              required
              autoFocus
              value={currentMessage}
              onChange={e => setCurrentMessage(e.target.value)}
            />
            <button>Send</button>
          </form>
        </div>
      </div>
    );
  }
}

ChatRoom.propTypes = {
  onlineUsers: PropTypes.array.isRequired,
  messages: PropTypes.array.isRequired,
  userName: PropTypes.string.isRequired,
  setCurrentMessage: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  currentMessage: PropTypes.string.isRequired,
};
