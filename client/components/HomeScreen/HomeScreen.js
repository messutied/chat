import React, { Component, PropTypes } from 'react';
import './HomeScreen.scss';

export default class HomeScreen extends Component {
  state = { userName: '', roomKey: null, roomFound: true, messages: [] };

  componentWillMount() {
    if (window.location.hash.indexOf('rooms/') !== -1) {
      const roomKey = window.location.hash.split('/')[1];
      fetch(`/rooms/${roomKey}`)
      .then(response => response.json())
      .then((resp) => {
        this.setState({ roomKey, messages: resp.messages });
      }).catch(() => {
        this.setState({ roomFound: false });
      });
    }
  }

  handleSubmit(evt) {
    evt.preventDefault();
    const { userName, roomKey, messages } = this.state;

    if (roomKey) {
      this.props.onChatStart(userName, roomKey, messages);
    } else {
      this.createChat();
    }
  }

  createChat() {
    fetch('/rooms', { method: 'POST' })
      .then(response => response.json())
      .then((room) => {
        window.location.hash = `rooms/${room.key}`;
        this.props.onChatStart(this.state.userName, room.key, []);
      });
  }

  render() {
    const { roomKey, roomFound } = this.state;
    return (
      <div className="home-screen">
        <h1>Welcome to Chat!</h1>
        <div className="info">
          {!roomFound ? <span className="error">Chat room not found! </span> : ''}
          {roomKey ? 'Joining a chat room' : 'Creating new chat room'}
          <br />
          Please enter your user name
        </div>
        <br />
        <form onSubmit={e => this.handleSubmit(e)}>
          <input
            type="text"
            required
            autoFocus
            placeholder="User name"
            className="user-name"
            value={this.state.userName}
            onChange={e => this.setState({ userName: e.target.value })}
          />
          <button>Lets Chat!</button>
        </form>
      </div>
    );
  }
}

HomeScreen.propTypes = {
  onChatStart: PropTypes.func.isRequired,
};
