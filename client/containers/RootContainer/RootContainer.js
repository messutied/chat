import React, { Component } from 'react';
import 'whatwg-fetch';
import HomeScreen from '../../components/HomeScreen/HomeScreen';
import ChatRoomScreen from '../../components/ChatRoomScreen/ChatRoomScreen';
import './RootContainer.scss';

export default class RootContainer extends Component {
  state = { currentScreen: 'HOME', userName: '', chatKey: null, messages: [] }

  onChatStart(userName, chatKey, messages) {
    this.setState({ userName, chatKey, messages });
  }

  render() {
    const { userName, chatKey } = this.state;
    return (
      <div className="container">
        {
          userName.length > 0 ?
            <ChatRoomScreen
              userName={userName}
              chatKey={chatKey}
              messages={this.state.messages}
              setUsername={nUserName => this.setState({ userName: nUserName })}
            /> :
            <HomeScreen onChatStart={(u, c, ms) => this.onChatStart(u, c, ms)} />
        }
      </div>
    );
  }
}
